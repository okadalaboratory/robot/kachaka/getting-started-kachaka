# Getting started - Kachaka
Kachakaを使う

# News
## 2024.05.16
カチャカソフトウェアアップデートのお知らせ（Ver 3.0.14）
- カチャカと自由にお話しする機能
- バッテリー情報の取得
- ToFセンサー画像の取得
- マップの切り替え
- フロント・バックLEDライトの点灯

[カチャカ本体のソフトウェアアップデート方法](https://kachaka.zendesk.com/hc/ja/articles/7009017457167)



# 公式情報
[Kachaka(カチャカ)](https://kachaka.life/)

[Amazonで購入する](https://www.amazon.co.jp/dp/B0C6LZ61FR)

- 本体：228,000円
- 月額使用料：980円／月

## サポートサイト
- [Webマニュアル](https://kachaka.zendesk.com/hc/ja)
- [FAQ](https://kachaka.zendesk.com/hc/ja/categories/5484183913871-FAQ-%E3%82%88%E3%81%8F%E3%81%82%E3%82%8B%E3%81%94%E8%B3%AA%E5%95%8F-)

## 遠隔操作機能
[遠隔操作機能](https://kachaka.zendesk.com/hc/ja/articles/7615186496911-%E9%81%A0%E9%9A%94%E6%93%8D%E4%BD%9C%E6%A9%9F%E8%83%BD?_gl=1*hjf2q7*_ga*NjQ0NzY2NjQyLjE2OTQ2NTAyNDQ.*_ga_NYXJ9742V3*MTY5NDgyNzI1Ny4yLjEuMTY5NDgyNzQ3NC4wLjAuMA..)


## 開発者向けAPI
[カチャカの外部開発者向けAPIが公開【第1弾】](https://note.kachaka.life/n/nbb782c0fecde?_gl=1%2a1a1jxfu%2a_gcl_au%2aMTcwNDYwMjI2OS4xNjk0NjUwMjQ0%2a_ga%2aNjQ0NzY2NjQyLjE2OTQ2NTAyNDQ.%2a_ga_NYXJ9742V3%2aMTY5NDgyNzI1Ny4yLjAuMTY5NDgyNzI5NC4wLjAuMA..)

[kachaka-api(GitHub)](https://github.com/pf-robotics/kachaka-api)

[マニュアル、チュートリアル](https://github.com/pf-robotics/kachaka-api/blob/main/README.md)

# 開発者
[近藤さん](https://www.youtalk.jp/)

[kachakaをROS2のnavigation2から動かす](https://qiita.com/terakoji-pfr/items/0f1535b45fda58edad83)

[kachaka-api でカチャカを動かす](https://qiita.com/ksyundo/items/f10f9a6c704442bb3a10)

# 報道

[人の指示で家具を動かすスマートファニチャー・プラットフォーム「カチャカ」2023年5月17日（水）新発売](https://prtimes.jp/main/html/rd/p/000000005.000115855.html)


# SNS
[Instagram](https://www.instagram.com/kachaka_jp/)




## Twitter

[カチャカ](https://twitter.com/search?q=%E3%82%AB%E3%83%81%E3%83%A3%E3%82%AB&src=typed_query)

[kachaka]()

[@kachaka_jp](https://twitter.com/kachaka_jp?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor)

## Qiita
[kachaka](https://qiita.com/tags/kachaka)

[カチャカ](https://qiita.com/tags/%e3%82%ab%e3%83%81%e3%83%a3%e3%82%ab)